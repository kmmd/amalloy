from copy import deepcopy


__all__ = ["chemical", "alloy"]


class chemical:
    symbols = (
        "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al",
        "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn",
        "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb",
        "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In",
        "Sn", "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm",
        "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta",
        "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At",
        "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk",
        "Cf", "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt",
        "Ds", "Rg", "Cn", "Uut", "Fl", "Uup", "Lv", "Uus", "Uuo")


class alloy(chemical):
    @staticmethod
    def get(pkg, symbol):
        """
        Gets the value of the requested symbol from the JSON-formatted
        package.

        Parameters
        ----------
        pkg : dict
            JSON-formatted settings for amalloy functions.

        symbol : str
            Chemical symbol whose fraction/percentage is desired.

        Returns
        -------
        float
            Quantity for the given symbol.
        """
        elements = pkg.get("alloying elements", dict())
        try:
            return elements[symbol]
        except KeyError:
            if symbol == pkg.get("base element", None):
                alloying = sum(map(float, elements.values()))
                if alloying > 1:
                    return 100. - alloying
                else:
                    return 1. - alloying
            else:
                return 0.

    @staticmethod
    def set(pkg, base=None, **elements):
        """
        Sets the given elements. The base element, if provided, is ignored
        since it constitutes the balance of alloy composition.

        Parameters
        ----------
        pkg : dict
            JSON-formatted settings used by amalloy functions.

        elements : dict
            Chemical symbols and their quantities to be set.

        Returns
        -------
        dict
            Updated copy of the data package.

        Example
        -------

        >>> composition.set(base="Al", Mg=4.85, Cr=0.1)
            {
                "base element": "Al",
                "alloying elements": {
                    "Mg": 4.85,
                    "Cr": 0.1
                }
            }

        This can be used to either create a base JSON package or to update
        an existing package. For example, if `existing` is (as the name implies)
        an existing JSON-formatted data package,

        >>> existing.update(alloy.set(Mg=4.85, Cr=0.1, base="Al"))

        or, equivalently,

        >>> alloy.set(existing, Mg=4.85, Cr=0.1, base="Al")
        """
        out = deepcopy(pkg)
        if base and (base in chemical.symbols):
            out["base element"] = base
        allowed = set(chemical.symbols) - {pkg.get("base element", None)}
        out["alloying elements"] = out.get("alloying elements", dict())
        out["alloying elements"].update(
            {k:v for k,v in elements.items() if k in allowed}
        )
        return out
