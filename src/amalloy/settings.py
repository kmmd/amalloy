from copy import deepcopy


__all__ = ["settings"]


class settings:
    @staticmethod
    def set(pkg=dict(), **opts):
        """
        Sets they key/value pairs in the JSON-formatted settings package. Use
        `options.set` to update individual function options.

        Parameters
        ----------
        pkg : dict (optional)
            JSON-formatted settings dictionary used by amalloy functions.

        opts : dict
            Key/value pairs that are to be set.

        Returns
        -------
        dict
            The updated package.
        """
        out = deepcopy(pkg)
        out.update(opts)
        return out
