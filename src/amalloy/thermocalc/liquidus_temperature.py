from .. import options
from copy import deepcopy
from tc_python import *

import logging
_logger = logging.getLogger(__name__)

__all__ = ["liquid_temperature"]


def liquidus_temperature(opts):
    """
    Calculate the liquidus temperature of an alloy. All settings are passed as a
    JSON-formatted package/dictionary. An example of this file:

        {
            "base element": "Al",
            "alloying elements": {
                "Mg": 4.0,
                "Cr": 1.0,
                "Mn": 2.0
            }
            "options": {
                "yield strength": {
                    "Matrix": "FCC_A1",
                    ...
                },
                "liquidus temperature": {
                    "suspendGasPhase": "True",
                    ...
                }
            }
        }

    Parameters
    ----------
    alloying elements : dict[str, float] (optional)
        Alloying elements.The dictionary keys are the chemical symbols,
        the values are their stoichiometries. Default: No alloying elements.

    base element : str (optional)
        Element that balances the composition. Default: Al

    TC database : str (optional)
        Thermocalc database to use. Default: TCAL7

    Options
    -------
    liquidus temperature : dict[str, str] (optional)
        These options are stored in opts["options"]["liquidus temperature"].
        These are the argument settings to the calculator. Default options:
            {
                "suspendGasPhase": "True",
                "calculateOnlyLiquidus": "False",
                "upperTemperatureLimit": "2500",
                "gasPhase": "None",
                "globalMinForSol": "True",
                "liquidPhase": "LIQUID",
                "globalMinForLiq": "True",
                "maxNumberOfIterations": "10"
            }

    Returns
    -------
    dict
        This function returns an copy of the input options JSON package
        updated with "liquidus temperature".
    """
    # get the values this function needs from the input options
    alloying = dict(opts.get("alloying elements", dict()))
    base = str(opts.get("base element", "Al"))
    db = str(opts.get("TC database", "TCAL7"))

    # function/calculator-specific options
    fopts = {
        "suspendGasPhase": "True",
        "calculateOnlyLiquidus": "False",
        "upperTemperatureLimit": "2500",
        "gasPhase": "None",
        "globalMinForSol": "True",
        "liquidPhase": "LIQUID",
        "globalMinForLiq": "True",
        "maxNumberOfIterations": "10"
    }
    fopts.update(options.get("liquidus temperature", opts))

    with TCPython() as session:
        # build the ThermoCalc system
        system = (session
            .set_cache_folder(os.path.basename(__file__) + "_cache")
            .select_database_and_elements(
                    db,
                    [base] + list(alloying.keys())
                )
            ) # need to add number of allowable phases
            .get_system()
        )
        _logger.info("Available property models in the default property model "
                     "directory: {}".format(session.get_property_models())
        )
        # create the calculator
        calc = system.with_property_model_calculation(
            "Liquidus and Solidus Temperature"
        )
        calc.set_composition_unit(CompositionUnit.MASS_PERCENT)
        for element, quantity in alloying.items():
            calc.set_composition(element, quantity)

        # asking for the available arguments, if we do not change them,
        # they will be set to the default defined in the model
        _logger.info("Liquidus temperature available arguments: {}".format(
                calc.get_arguments()
            )
        )
        _logger.info("{} description: {}".format(
                'maxNumberOfIterations',
                calc.get_argument_description('maxNumberOfIterations')
            )
        )
        _logger.info("{} default: {}".format(
                'maxNumberOfIterations',
                calc.get_argument_default('maxNumberOfIterations')
            )
        )

        # set calculator options
        for k,v in fopts.items():
            _logger.info("{}: {}".format(k, v))
            calc.set_argument(k, v)
        # ##### RUN ##### #
        result = calc.calculate()

        # log the results
        _logger.info("Available result quantities: {}".format(
                result.get_result_quantities()
            )
        )
        _logger.info("Description of '{}': {}".format(
                "Liquidus temperature",
                result.get_result_quantity_description("Liquidus temperature")
            )
        )
        liquidus_temp = float(result.get_value_of("Liquidus temperature"))
        _logger.info("Liquidus temperature: {} degrees C".format(liquidus_temp))

        # save the results
        output = deepcopy(opts)
        output["liquidus temperature"] = liquidus_temp
        return output
