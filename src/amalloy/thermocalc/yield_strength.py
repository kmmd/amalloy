from .. import options
from copy import deepcopy
from tc_python import *

import logging
_logger = logging.getLogger(__name__)


__all__ = ["yield_strength"]


def yield_strength(opts):
    """
    Calculates the yield strength of a alloy. All settings are passed as a
    JSON-formatted package/dictionary. An example of this file:

        {
            "base element": "Al",
            "alloying elements": {
                "Mg": 4.0,
                "Cr": 1.0,
                "Mn": 2.0
            }
            "options": {
                "yield strength": {
                    "Matrix": "FCC_A1",
                    ...
                },
                "liquidus temperature": {
                    "suspendGasPhase": "True",
                    ...
                }
            },
            "temperature": 25.0
        }

    Parameters
    ----------
    alloying elements : dict[str, float] (optional)
        Alloying elements.The dictionary keys are the chemical symbols,
        the values are their stoichiometries. Default: No alloying elements.

    base element : str (optional)
        Element that balances the composition. Default: Al

    TC database : str (optional)
        Thermocalc database to use. Default: TCAL7

    temperature : float (optional)
        Temperature in degrees Celsius. Default: 25

    Options
    -------
    yield strength : dict[str, str] (optional)
        These options are stored in opts["options"]["yield strength"]. These
        are the argument settings to the ThermoCalc calculator. Default
        options:
            {
                "Matrix": "FCC_A1",
                "sol_str_selection": "True",
                "sol_str_temp_selection": "False",
                "w factor": "0.2919",
                "Solid solution strengthening temperature": "293.15",
                "grain_str_selection": "True",
                "Grain size in mu": "80",
                "User_Hall-Petch_selection": "True",
                "Hall-Petch constant": "220",
                'precip_str_selection': 'True',
                "A factor": "0.7639",
                'Anti-phase boundary energy Seidman': "0.5",
                'Anti-phase boundary energy Reppich': "0.142",
                "Precipitate": "AL6Mn",
                "Precipitation model list": "Simplified model (general)",
                "prefactor simplified model": "7.55e-06",
                "prefactor 'm'": "0.85",
                "seidman alpha": "2.6",
                "Precipitate radius seid": "1e-08",
                "Precipitate radius simp": "1e-08",
                "Equilibrium_minimization_strategy_id": "Local minimization preferred",
                "Constant strength addition": "0",
                "Critical radius simplified model": "2.88e-09",
                "Precipitate radius repp": "1e-08",
            }

    Returns
    -------
    dict
        This function returns a copy of the input options JSON package
        updated with

            "yield strength"
            "intrinsic yield strength"
            "solid solution strengthening"
            "grain boundary strengthening"
    """
    # get values from the input options
    alloying = dict(opts.get("alloying elements", dict()))
    base = str(opts.get("base element", "Al"))
    db = str(opts.get("TC database", "TCAL7"))
    temperature = float(opts.get("temperature", 25)) + 273.15

    # function-specific options
    fopts = {
        "Matrix": "FCC_A1",
        "sol_str_selection": "True",
        "sol_str_temp_selection": "False",
        "w factor": "0.2919",
        "Solid solution strengthening temperature": "293.15",
        "grain_str_selection": "True",
        "Grain size in mu": "80",
        "User_Hall-Petch_selection": "True",
        "Hall-Petch constant": "220",
        "precip_str_selection": 'True',
        "A factor": "0.7639",
        'Anti-phase boundary energy Seidman': "0.5",
        'Anti-phase boundary energy Reppich': "0.142",
        "Precipitate": "AL6Mn",
        "Precipitation model list": "Simplified model (general)",
        "prefactor simplified model": "7.55e-06",
        "prefactor 'm'": "0.85",
        "seidman alpha": "2.6",
        "Precipitate radius seid": "1e-08",
        "Precipitate radius simp": "1e-08",
        "Equilibrium_minimization_strategy_id": "Local minimization preferred",
        "Constant strength addition": "0",
        "Critical radius simplified model": "2.88e-09",
        "Precipitate radius repp": "1e-08",
    }
    fopts.update(options.get("yield strength", opts))

    with TCPython() as session:
        system = (session
            .set_cache_folder(os.path.basename(__file__) + "_cache")
            .select_database_and_elements(
                db,
                [base] + list(alloying.keys())
            )
            .without_default_phases()
            .select_phase(fopts["Matrix"])
            .select_phase(fopts["Precipitate"])
            .get_system()
        )
        # this system builder included database and elements, as well as only
        # the fcc_a1 phase for now.

        # asking at first for the available property models and their name
        _logger.info("Available property models in the default property model "
                     "directory: {}".format(session.get_property_models())
        )
        calc = (system
            .with_property_model_calculation("Yield Strength")
            .set_temperature(temperature)
            .set_composition_unit(CompositionUnit.MASS_PERCENT)
        )

        for element, quantity in alloying.items():
            calc.set_composition(element, quantity)

        # asking for the available arguments, if we do not change them, they
        # will be set to the default defined in the model
        _logger.info("Available arguments: {}".format(calc.get_arguments()))
        _logger.info("{} description: {}".format(
                'User_Hall-Petch_selection',
                calc.get_argument_description('User_Hall-Petch_selection')
            )
        )
        _logger.info("{} default: {}".format(
                'User_Hall-Petch_selection',
                calc.get_argument_default('User_Hall-Petch_selection')
            )
        )

        # set calculator options
        for k,v in fopts.items():
            _logger.info("{}: {}".format(k, v))
            calc.set_argument(k, v)
        # ##### RUN ##### #
        result = calc.calculate()

        # log results
        _logger.info("Yield strength vailable results: {}".format(
                result.get_result_quantities()
            )
        )
        _logger.info("{} description: {}".format(
                "Solid solution strengthening FCC",
                result.get_result_quantity_description(
                    "Solid solution strengthening FCC"
                )
            )
        )

        yield_strength = result.get_value_of("Total yield strength")
        intrinsic_strength = result.get_value_of("Intrinsic strength")
        solid_sol_str_fcc = result.get_value_of(
            "Solid solution strengthening FCC"
        )
        grain_boundary_str = result.get_value_of(
            "Grain boundary strengthening"
        )
        _logger.info("Yield strength: {} MPa".format(yield_strength))
        _logger.info("Intrinsic yield strength: {} MPa".format(
                intrinsic_strength
            )
        )
        _logger.info("Solid solution strengthening FCC: {} MPa".format(
                solid_sol_str_fcc
            )
        )
        _logger.info("Grain boundary strengthening : {} MPa".format(
                grain_boundary_str
            )
        )

        # save the results
        output = deepcopy(opts)
        output["yield strength"] = yield_strength
        output["intrinsic yield strength"] = intrinsic_strength
        output["solid solution strengthening"] = solid_sol_str_fcc
        output["grain boundary strengthening"] = grain_boundary_str
        return output
