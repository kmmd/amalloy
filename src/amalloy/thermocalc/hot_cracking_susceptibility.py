from .. import options
from copy import deepcopy
from tc_python import *

import logging
_logger = logging.getLogger(__name__)


__all__ = ["hot_cracking_susceptibility"]


def hot_cracking_susceptibility(opts):
    """
    Calculates the hot cracking susceptibility of a alloy. All settings are
    passed as a JSON-formatted package/dictionary. An example of this file:

        {
            "base element": "Al",
            "alloying elements": {
                "Mg": 4.0,
                "Cr": 1.0,
                "Mn": 2.0
            }
            "options": {
                "hot cracking susceptibility": {
                    "Liquid phase": "LIQUID",
                    ...
                },
                "liquidus temperature": {
                    "suspendGasPhase": "True",
                    ...
                }
            }
        }

    Parameters
    ----------
    alloying elements : dict[str, float] (optional)
        Alloying elements.The dictionary keys are the chemical symbols,
        the values are their stoichiometries. Default: No alloying elements.

    base element : str (optional)
        Element that balances the composition. Default: Al

    TC database : str (optional)
        Thermocalc database to use. Default: TCAL7

    temperature : float (optional)
        Temperature in degrees Celsius. Default: 726.85

    Options
    -------
    hot cracking susceptibility : dict[str, str] (optional)
        These options are stored in
        opts["options"]["hot cracking susceptibility"]. These are the argument
        settings to the ThermoCalc calculator. Default options:
            {
                "Liquid phase": "LIQUID",
                "Liquid fraction for start of relaxation": "0.6",
                "Use global minimization": "False",
                "Liquid fraction for start of vulnerability": "0.1",
                "Smallest fraction": "1e-12",
                "Terminate at liquid amount": "0.01",
                "Show advanced Scheil options": "False",
                "Temperature step": "1.0",
                "Thermal mode selection": "Heat flow proportional to 1/sqrt(time)",
                "Start temperature": "2500",
                "Liquid fraction smallest for vulnerability": "0.01"
            }

    Returns
    -------
    dict
        This function returns an copy of the input options JSON package
        updated with "hot cracking coefficient".
    """
    # get values from the input options
    alloying = dict(opts.get("alloying elements", dict()))
    base = str(opts.get("base element", "Al"))
    db = str(opts.get("TC database", "TCAL7"))
    temperature = float(opts.get("temperature", 726.85)) + 273.15

    # function-specific options
    fopts = {
        "Liquid phase": "LIQUID",
        "Liquid fraction for start of relaxation": "0.6",
        "Use global minimization": "False",
        "Liquid fraction for start of vulnerability": "0.1",
        "Smallest fraction": "1e-12",
        "Terminate at liquid amount": "0.01",
        "Show advanced Scheil options": "False",
        "Temperature step": "1.0",
        "Thermal mode selection": "Heat flow proportional to 1/sqrt(time)",
        "Start temperature": "2500",
        "Liquid fraction smallest for vulnerability": "0.01"
    }
    fopts.update(options.get("hot cracking susceptibility", opts))

    # run TCPython
    with TCPython() as session:
        # this system builder included database and elements.
        system = (
            session
                .set_cache_folder(os.path.basename(__file__) + "_cache")
                .select_database_and_elements(
                    db,
                    [base] + list(alloying.keys())
                )
                .get_system()
        )

        # asking at first for the available property models and their name
        _logger.info("Available property models in the default property model "
                     "directory: {}".format(session.get_property_models()))
        calc = (system
            .with_property_model_calculation(
                "Crack Susceptibility Coefficient"
            )
            .set_temperature(temperature)
            .set_composition_unit(CompositionUnit.MASS_PERCENT)
        )

        for element, quantity in alloying.items():
            calc.set_composition(element, float(quantity))

        # asking for the available arguments, if we do not change them,
        # they will be set to the default defined in the model
        _logger.info("Available arguments: {}".format(calc.get_arguments()))
        _logger.info("{} description: {}".format(
                'Start temperature',
                calc.get_argument_description('Start temperature')
            )
        )
        _logger.info("{}: {}".format(
                'Start temperature',
                calc.get_argument_default('Start temperature')
            )
        )

        # set calculator options
        for k,v in fopts.items():
            _logger.info("{}: {}".format(k, v))
            calc.set_argument(k, v)
        # ##### RUN ##### #
        result = calc.calculate()

        # log the results
        _logger.info("Hot cracking result quantities: {}".format(
                result.get_result_quantities()
            )
        )
        _logger.info("{} description: {}".format(
                "Crack susceptibility coefficient",
                result.get_result_quantity_description(
                    "Crack Susceptibility Coefficient"
                )
            )
        )
        hot_cracking_coeff = result.get_value_of(
            "Crack Susceptibility Coefficient"
        )
        _logger.info("Crack susceptibility coefficient: {} ".format(
                hot_cracking_coeff
            )
        )

        # save the results
        output = deepcopy(opts)
        output["hot cracking susceptibility coefficient"] = hot_cracking_coeff
        return output
