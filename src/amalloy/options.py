from copy import deepcopy


__all__ = ["options"]


class options:
    @staticmethod
    def get(func, pkg):
        """
        Gets the dictionary of options for an amalloy function from
        the JSON-package.

        Parameters
        ----------
        func : str
            Function name whose options are to be returned. If not found, an
            empty dictionary (no options) is returned.

        pkg : dict
            JSON-formatted package used by amalloy functions.

        Returns
        -------
        dict
            Options dictionary.
        """
        return pkg.get("options", dict()).get(func, dict())

    @staticmethod
    def set(func, pkg=dict(), **opts):
        """
        Sets the option for the given function in the JSON-package
        configuration for the amalloy function.

        Parameters
        ----------
        func : str
            Function name whose options are to be set.

        pkg : dict (optional)
            JSON-formatted settings package used by amalloy functions.

        opts : dict
            key/value pairs to be set in the options section.

        Returns
        -------
        dict
            Copy of the package with updated options, so these can be nested.
        """
        if func is None:
            raise ValueError("A value for `func` must be given.")
        out = deepcopy(pkg)
        out["options"] = out.get("options", dict())
        out[func] = out["options"].get(func, dict())
        out[func].update(opts)
        return out
