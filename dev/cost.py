import os
import json

import amalloy
from amalloy import thermocalc as tc


def make_workdir(prefix="calphad"):
    """
    Create a working directory. This function is not thread safe.
    """
    count = 0
    filename = lambda n: f"{prefix}-{n:04d}"
    while os.path.isdir(filename(count)):
        count += 1
    dir = filename(count)
    os.mkdir(dir)
    return dir


def figure_of_merit(composition):
    """
    Calculate the figure of merit from a composition.

    Parameters
    ----------
    composition : dict
        Elemental composition, e.g. {'Mg': 4.85, 'Cr': 0.1}. The base
        is assumed to be aluminum.

    Returns
    -------
    float
        Scalar value for the figure of merit for this composition.
    """
    # path manipulations
    homedir = os.path.abspath(os.curdir)
    workdir = make_workdir()
    os.chdir(workdir)
    # perform the work to calculate the merit function
    pkg = amalloy.alloy.set(**composition, base="Al")
    pkg = tc.liquidus_temperature(pkg)
    # pkg = tc.driving_force(pkg)
    # pkg = tc.hot_cracking_susceptibility(pkg)
    # pkg = tc.yield_strength(pkg)
    # calculate the merit function
    # these values should be scaled so they are all a similar magnitude
    liq = pkg.get("liquidus temperature", 0.0)/1000. # assume a magnitude ~1000
    hcc = pkg.get("hot cracking susceptibility coefficient", 100.0)/10. # assume magnitude ~ 10
    ys = pkg.get("yield strength", 0.0)/100. # assume magnitude ~ 100
    # #### FOM ##### #
    result = -liq - hcc + ys
    pkg["figure of merit"] = result
    # save the results
    with open("results.json", "w") as ofs:
        json.dump(pkg, ofs)
    # move back to where we started
    os.chdir(homedir)
    # done, return the result
    return result
